package DZ2_PM.task1;
//Реализовать метод, который на вход принимает ArrayList<T>, а возвращает
//набор уникальных элементов этого массива. Решить используя коллекции.
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
public class main {
    public static <T> void main(String[] args) {
        List<Integer> generic = Arrays.asList(0, 0, 1, 1, 2, 3, 4, 5, 5);
        convert(generic);
    }
    public static <T> HashSet<T> convert(List<T> from) {
        return new HashSet<>(from);
    }
}
