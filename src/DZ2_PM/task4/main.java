package DZ2_PM.task4;
//В некоторой организации хранятся документы (см. класс Document). Сейчас все
//документы лежат в ArrayList, из-за чего поиск по id документа выполняется
//неэффективно. Для оптимизации поиска по id, необходимо помочь сотрудникам
//перевести хранение документов из ArrayList в HashMap.
//public class Document {
//public int id;
//public String name;
//public int pageCount;
//}
//Реализовать метод со следующей сигнатурой:
//public Map<Integer, Document> organizeDocuments(List<Document> documents)
import java.util.Arrays;
import java.util.List;
public class main {
    public static void main(String[] args) {
        Document doc1 = new Document(1, "резюме Бондаренко Ксения Михайловна", 50);
        Document doc2 = new Document(2, "резюме Васильева Елена Евнгеньевна", 1);
        List<Document> documents = Arrays.asList(doc1, doc2);
        DocumentOrganizer res = new DocumentOrganizer();
        System.out.println(res.organizeDocument(documents));
    }
}
