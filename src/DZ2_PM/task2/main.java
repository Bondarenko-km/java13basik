package DZ2_PM.task2;
//С консоли на вход подается две строки s и t. Необходимо вывести true, если
//одна строка является валидной анаграммой другой строки и false иначе.
//Анаграмма — это слово или фраза, образованная путем перестановки букв
//другого слова или фразы, обычно с использованием всех исходных букв ровно
//один раз.
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Set;
public class main {
    public static void main(String[] args) {
        System.out.println(isAnagram1("клоака", "околка"));
        System.out.println(isAnagram1("бейсбол", "бобслей"));
        System.out.println(isAnagram("бейсбол", "бобслей"));
        System.out.println(isAnagram("бейсбол", "бобслей"));
    }
    public static boolean isAnagramStream(String s, String t) {
        return Arrays.equals(s.chars().sorted().toArray(), t.chars().sorted().toArray());
    }
    private static boolean isAnagram(String s, String t) {
        if (s.length() != t.length()) {
            return false;
        }
        Set<Character> setFirst = new LinkedHashSet<>();
        for (char ch: s.toCharArray()) {
            setFirst.add(ch);
        }
        Set<Character> setSecond = new LinkedHashSet<>();
        for (char ch: s.toCharArray()) {
            setSecond.add(ch);
        }
        return setFirst.equals(setSecond);
    }
    public static boolean isAnagram1(String str1, String str2) {
        if (str1.length() != str2.length())  {
            return false;
        }
        HashMap<Character, Integer> s1 = new HashMap<>();
        for (char element: str1.toLowerCase().toCharArray()) {
            s1.put(element, s1.getOrDefault(element, 0) + 1);
        }
        HashMap<Character, Integer> s2 = new HashMap<>();
        for (char element: str2.toLowerCase().toCharArray()) {
            s1.put(element, s2.getOrDefault(element, 0) + 1);
        }
        return s1.equals(s2);
    }
}
