package DZ2_PM.task3;

import java.util.Set;
public class PowerfulSet {
    public <T>Set<T> intersection(Set<T> set1, Set<T> set2) {
        set1.retainAll(set2);
        return set1;
    }
    public <T>Set<T> union(Set<T> set1, Set<T> set2) {
        set1.addAll(set2);
        return set1;
    }
    public <T>Set<T> relativeCompliment(Set<T> set1, Set<T> set2) {
        set1.removeAll(set2);
        return set1;

    }
}
