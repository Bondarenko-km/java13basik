package DZ2_PM.task3;
//Реализовать класс PowerfulSet, в котором должны быть следующие методы:
//a. public <T> Set<T> intersection(Set<T> set1, Set<T> set2) — возвращает
//пересечение двух наборов. Пример: set1 = {1, 2, 3}, set2 = {0, 1, 2, 4}.
//Вернуть {1, 2}
//b. public <T> Set<T> union(Set<T> set1, Set<T> set2) — возвращает
//объединение двух наборов. Пример: set1 = {1, 2, 3}, set2 = {0, 1, 2, 4}.
//Вернуть {0, 1, 2, 3, 4}
//c. public <T> Set<T> relativeComplement(Set<T> set1, Set<T> set2) —
//возвращает элементы первого набора без тех, которые находятся также
//и во втором наборе. Пример: set1 = {1, 2, 3}, set2 = {0, 1, 2, 4}. Вернуть {3}
import java.util.HashSet;
import java.util.Set;
public class main {
    public static void main(String[] args) {
        PowerfulSet set = new PowerfulSet();
        Set<Integer> wet = new HashSet<>();
        wet.add(1);
        wet.add(2);
        wet.add(3);
        Set<Integer> tet = new HashSet<>();
        tet.add(0);
        tet.add(1);
        tet.add(2);
        tet.add(4);
        System.out.println(set.intersection(wet, tet));
        Set<Integer> wet2 = new HashSet<>();
        wet2.add(1);
        wet2.add(2);
        wet2.add(3);
        Set<Integer> tet2 = new HashSet<>();
        tet2.add(0);
        tet2.add(1);
        tet2.add(2);
        tet2.add(4);
        System.out.println(set.union(wet2, tet2));
        Set<Integer> wet3 = new HashSet<>();
        wet3.add(1);
        wet3.add(2);
        wet3.add(3);
        Set<Integer> tet3 = new HashSet<>();
        tet3.add(0);
        tet3.add(1);
        tet3.add(2);
        tet3.add(4);
        System.out.println(set.relativeCompliment(wet3, tet3));
    }
}
