package FilmLibrary.service;
import myproject.filmoteka.dto.AddDirectorDTO;
import myproject.filmoteka.dto.DirectorDTO;
import myproject.filmoteka.dto.DirectorSearchDTO;
import myproject.filmoteka.dto.DirectorWithFilmsDTO;
import myproject.filmoteka.mapper.DirectorMapper;
import myproject.filmoteka.mapper.DirectorWithFilmsMapper;
import myproject.filmoteka.model.Director;
import myproject.filmoteka.model.Film;
import myproject.filmoteka.repository.DirectorRepository;
import myproject.filmoteka.repository.FilmRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jdbc.core.JdbcAggregateOperations;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.util.List;

@Service
public class DirectorService extends GenericService<Director, DirectorDTO> {

    private final FilmRepository filmRepository;

    private final DirectorWithFilmsMapper directorWithFilmsMapper;
    private final DirectorRepository directorRepository;

//    private final DirectorRepository directorRepository;
//    private final FilmService filmService;




    public DirectorService(DirectorMapper directorMapper,
                           FilmRepository filmRepository, DirectorWithFilmsMapper directorWithFilmsMapper, DirectorRepository directorRepository) {
        super(directorRepository, directorMapper);
        this.filmRepository = filmRepository;
        this.directorWithFilmsMapper = directorWithFilmsMapper;
        this.directorRepository = directorRepository;
    }



//    public DirectorService(DirectorRepository directorRepository,
//                           DirectorMapper directorMapper,
//                           FilmService filmService, FilmService service) {
//        super(directorRepository, directorMapper);
//        this.directorRepository = directorRepository;
//        this.filmService = filmService;
//    }




    public Page<DirectorDTO> searchDirectors(final String fio,
                                             Pageable pageable) {
        Page<Director> authors = (Page<Director>) directorRepository.findAllByDirectorFioContainsIgnoreCaseAndIsDeletedFalse(fio, pageable);
        List<DirectorDTO> result = mapper.toDTOs(authors.getContent());
        return new PageImpl<>(result, pageable, authors.getTotalElements());
    }


    public DirectorDTO addFilm(Long directorId, Long filmId) {
        Director director = repository.findById(directorId).orElseThrow(() -> new NotFoundException("Режиссёр не найден"));
        Film film = filmRepository.findById(filmId).orElseThrow(() -> new NotFoundException("Фильм не найден"));
        director.getFilms().add(film);
        return mapper.toDTO(repository.save(director));
    }

    public Page<DirectorWithFilmsDTO> getAllDirectorsWithFilms(Pageable pageable) {
        Page<Director> directorsPaginated = repository.findAll(pageable);
        List<DirectorWithFilmsDTO> result = directorWithFilmsMapper.toDTOs(directorsPaginated.getContent());
        return new PageImpl<>(result, pageable, directorsPaginated.getTotalElements());
    }

    public DirectorWithFilmsDTO getDirectorWithFilms(Long id) {
        return directorWithFilmsMapper.toDTO(mapper.toEntity(super.getOne(id)));
    }

    public Page<DirectorWithFilmsDTO> findDirectors(DirectorSearchDTO directorSearchDTO, Pageable pageable) {
        Page<Director> directorsPaginated = ((DirectorRepository)repository).searchDirectors(
                directorSearchDTO.getDirectorFio(), directorSearchDTO.getFilmTitle(), pageable);
        List<DirectorWithFilmsDTO> result = directorWithFilmsMapper.toDTOs(directorsPaginated.getContent());
        return new PageImpl<>(result, pageable, directorsPaginated.getTotalElements());
    }

    public void addFilm(AddDirectorDTO addDirectorDTO) {
    }
}
