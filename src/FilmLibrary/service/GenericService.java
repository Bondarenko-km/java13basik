package FilmLibrary.service;
import myproject.filmoteka.dto.GenericDTO;
import myproject.filmoteka.expection.MyDeleteException;
import myproject.filmoteka.mapper.GenericMapper;
import myproject.filmoteka.model.GenericModel;
import myproject.filmoteka.repository.GenericRepository;
import org.springdoc.core.converters.models.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.time.LocalDateTime;
import java.util.List;

@Service
public abstract class GenericService<T extends GenericModel, N extends GenericDTO> {

    protected final GenericRepository<T> repository;
    protected final GenericMapper<T, N> mapper;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    protected GenericService(GenericRepository<T> repository, GenericMapper<T, N> mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    public List<N> listAll() {
        return mapper.toDTOs(repository.findAll());
    }

    public Page<N> listAll(Pageable pageable) {
        Page<T> objects = repository.findAll((org.springframework.data.domain.Pageable) pageable);
        List<N> result = mapper.toDTOs(objects.getContent());
        return new PageImpl<>(result, (org.springframework.data.domain.Pageable) pageable, objects.getTotalElements());
    }

    public Page<N> listAllNotDeleted(Pageable pageable) {
        Page<T> preResult = repository.findAllByIsDeletedFalse(pageable);
        List<N> result = mapper.toDTOs(preResult.getContent());
        return new PageImpl<>(result, (org.springframework.data.domain.Pageable) pageable, preResult.getTotalElements());
    }

    public List<N> listAllNotDeleted() {
        return mapper.toDTOs(repository.findAllByIsDeletedFalse());
    }


    public N getOne(final Long id) {
        return mapper.toDTO(repository.findById(id).orElseThrow(() -> new NotFoundException("Данные по заданному id: " + id + " не найдены")));
    }


    public N create(N newObject) {
        return mapper.toDTO(repository.save(mapper.toEntity(newObject)));
    }

    public N update(N updatedObject) {
        return mapper.toDTO(repository.save(mapper.toEntity(updatedObject)));
    }

    public void delete(Long id) {
        repository.deleteById(id);
    }

    public void deleteSoft(long l) {
    }


    /**
     * Восстановление помеченной записи, как удаленной.
     *
     * @param id - идентификатор сущности, которая должна быть восстановлена
     */
    public void restore(Long id) {
        T obj = repository.findById(id).orElseThrow(() -> new NotFoundException("Объект не найден с айди: " + id));
        unMarkAsDeleted(obj);
        repository.save(obj);
    }



    /***
     * Хард Удаление сущности из БД.
     *
     * @param id - идентификатор сущности, которая должна быть удалена.
     */


    public void deleteHard(Long id) throws MyDeleteException {
        repository.deleteById(id);
    }
    public void markAsDeleted(GenericModel genericModel) {
        genericModel.setDeleted(true);
        genericModel.setDeletedWhen(LocalDateTime.now());
        genericModel.setDeletedBy(SecurityContextHolder.getContext().getAuthentication().getName());
    }

    public void unMarkAsDeleted(GenericModel genericModel) {
        genericModel.setDeleted(false);
        genericModel.setDeletedWhen(null);
        genericModel.setDeletedBy(null);
    }



}

