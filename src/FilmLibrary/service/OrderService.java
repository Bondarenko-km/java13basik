package FilmLibrary.service;
import myproject.filmoteka.dto.OrderDTO;
import myproject.filmoteka.mapper.OrderMapper;
import myproject.filmoteka.model.Film;
import myproject.filmoteka.model.Order;
import myproject.filmoteka.model.User;
import myproject.filmoteka.repository.FilmRepository;
import myproject.filmoteka.repository.OrderRepository;
import myproject.filmoteka.repository.UserRepository;
import myproject.filmoteka.service.GenericService;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;


@Service
public class OrderService extends GenericService<Order, OrderDTO> {

    private final UserRepository userRepository;
    private final FilmRepository filmRepository;

    public OrderService(OrderRepository orderRepository, OrderMapper orderMapper,
                        UserRepository userRepository,
                        FilmRepository filmRepository) {
        super(orderRepository, orderMapper);
        this.userRepository = userRepository;
        this.filmRepository = filmRepository;
    }

    public OrderDTO buyFilm(Long userId, Long filmId) {
        User user = userRepository.findById(userId).orElseThrow(() -> new NotFoundException("Пользователь не найден"));
        Film film = filmRepository.findById(filmId).orElseThrow(() -> new NotFoundException("Фильм не найден"));
        Order order = new Order();
        order.setUser(user);
        order.setFilm(film);
        return mapper.toDTO(repository.save(order));
    }
}
