package FilmLibrary.service;
import myproject.filmoteka.dto.FilmDTO;
import myproject.filmoteka.dto.FilmSearchDTO;
import myproject.filmoteka.dto.FilmWithDirectorsDTO;
import myproject.filmoteka.mapper.FilmMapper;
import myproject.filmoteka.mapper.FilmWithDirectorsMapper;
import myproject.filmoteka.model.Director;
import myproject.filmoteka.model.Film;
import myproject.filmoteka.repository.DirectorRepository;
import myproject.filmoteka.repository.FilmRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;


import java.util.List;

@Service
public class FilmService extends GenericService<Film, FilmDTO> {

    private final DirectorRepository directorRepository;
    private final FilmWithDirectorsMapper filmWithDirectorsMapper;
    private FilmRepository filmRepository;
    protected FilmService(FilmRepository filmRepository, FilmMapper filmMapper,
                          DirectorRepository directorRepository, FilmWithDirectorsMapper filmWithDirectorsMapper) {
        super(filmRepository, filmMapper);
        this.directorRepository = directorRepository;
        this.filmWithDirectorsMapper = filmWithDirectorsMapper;
        this.filmRepository = filmRepository;
    }


    public FilmDTO addDirector(Long filmId, Long directorId) {
        Film film = repository.findById(filmId).orElseThrow(() -> new NotFoundException("Фильм не найден"));
        Director director = directorRepository.findById(directorId).orElseThrow(() -> new NotFoundException("Режиссёр не найден"));
        film.getDirectors().add(director);
        return mapper.toDTO(repository.save(film));
    }

    public Page<FilmWithDirectorsDTO> getAllFilmsWithDirectors(Pageable pageable) {
        Page<Film> filmsPaginated = repository.findAll(pageable);
        List<FilmWithDirectorsDTO> result = filmWithDirectorsMapper.toDTOs(filmsPaginated.getContent());
        return new PageImpl<>(result, pageable, filmsPaginated.getTotalElements());
    }

    public Page<FilmWithDirectorsDTO> findFilms(FilmSearchDTO filmSearchDTO, Pageable pageable) {
        String genre = filmSearchDTO.getGenre() != null ? String.valueOf(filmSearchDTO.getGenre().ordinal()) : null;
        Page<Film> filmsPaginated = ((FilmRepository)repository).searchFilms(genre,
                filmSearchDTO.getFilmTitle(), filmSearchDTO.getDirectorFio(), pageable);
        List<FilmWithDirectorsDTO> result = filmWithDirectorsMapper.toDTOs(filmsPaginated.getContent());
        return new PageImpl<>(result, pageable, filmsPaginated.getTotalElements());
    }

    public FilmWithDirectorsDTO getFilmWithDirectors(Long id) {
        return filmWithDirectorsMapper.toDTO(mapper.toEntity(super.getOne(id)));
    }
}
