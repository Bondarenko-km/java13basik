package FilmLibrary.expection;

public class MyDeleteException extends Exception {

    public MyDeleteException(String message) {
        super(message);
    }
}
