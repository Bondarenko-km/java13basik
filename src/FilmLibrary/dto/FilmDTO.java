package FilmLibrary.dto;
import myproject.filmoteka.model.Film;
import myproject.filmoteka.model.Genre;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FilmDTO
        extends GenericDTO {
    private String title;
    private Integer premierYear;
    private String country;
    private Genre genre;
    private Set<Long> ordersIds;

    private Set<Long> filmsIds;

    public <E> FilmDTO(String title2, String premierYear2, int i, int i1, String country2, String onlineCopyPath2, String publish2, String description2, Genre fantasy, HashSet<E> es, boolean b) {
    }
}
