package FilmLibrary.dto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO extends GenericDTO {

    private String login;
    private String password;
    private String firstName;
    private String lastName;
    private String middleName;
    private String birthDate;
    private String phone;
    private String address;
    private String email;
    private LocalDateTime createdWhen;
    private RoleDTO role;
    private String changePasswordToken;
    private Set<Long> ordersIds;

    public <E> UserDTO(String login, String password, String email, String birthDate, String firstName, String lastName, String middleName, String phone, String address, RoleDTO roleDTO, String changePasswordToken, HashSet<E> es) {
    }
}

