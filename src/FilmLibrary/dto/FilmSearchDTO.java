package FilmLibrary.dto;
import myproject.filmoteka.model.Genre;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FilmSearchDTO {

    private String filmTitle;
    private String directorFio;
    private Genre genre;
}
