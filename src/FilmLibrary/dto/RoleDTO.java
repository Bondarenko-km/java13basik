package FilmLibrary.dto;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class RoleDTO {

    private Long id;
    private String title;
    private String description;
}