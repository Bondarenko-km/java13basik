package FilmLibrary.dto;
import lombok.*;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class DirectorDTO extends GenericDTO {
    private String directorFio;
    private String position;
    private Set<Long> directorsIds;
    private boolean isDeleted;

    public <E> DirectorDTO(String restTestAuthorFio, String s, String testDescription, HashSet<E> es, boolean b) {
    }

}
