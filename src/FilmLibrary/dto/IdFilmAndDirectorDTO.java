package FilmLibrary.dto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class IdFilmAndDirectorDTO {

    private Long filmId;
    private Long directorId;
}
