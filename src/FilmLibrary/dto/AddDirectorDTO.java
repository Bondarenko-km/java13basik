package FilmLibrary.dto;
import lombok.*;


@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class AddDirectorDTO {
    Long filmId;
    Long directorId;
}