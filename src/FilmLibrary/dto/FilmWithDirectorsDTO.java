package FilmLibrary.dto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import myproject.filmoteka.model.Film;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FilmWithDirectorsDTO extends FilmDTO {

    private Set<DirectorDTO> directors;

    public FilmWithDirectorsDTO(Film film2, Set<FilmDTO> directors) {
    }
}