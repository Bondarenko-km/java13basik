package FilmLibrary.mapper;
import myproject.filmoteka.dto.GenericDTO;
import myproject.filmoteka.model.GenericModel;
import myproject.filmoteka.service.GenericService;
import java.util.List;

public interface Mapper<E extends GenericModel, D extends GenericDTO> {

    E toEntity(D dto);

    D toDTO(E entity);

    List<E> toEntities(List<D> dtos);

    List<D> toDTOs(List<E> entities);
}