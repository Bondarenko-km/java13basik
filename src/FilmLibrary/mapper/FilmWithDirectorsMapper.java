package FilmLibrary.mapper;
import myproject.filmoteka.dto.FilmWithDirectorsDTO;
import myproject.filmoteka.model.Film;
import myproject.filmoteka.model.GenericModel;
import myproject.filmoteka.repository.DirectorRepository;
import myproject.filmoteka.repository.OrderRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class FilmWithDirectorsMapper extends GenericMapper<Film, FilmWithDirectorsDTO> {

    private final DirectorRepository directorRepository;
    private final OrderRepository orderRepository;

    public FilmWithDirectorsMapper(ModelMapper modelMapper, DirectorRepository directorRepository,
                                   OrderRepository orderRepository) {
        super(modelMapper, Film.class, FilmWithDirectorsDTO.class);
        this.directorRepository = directorRepository;
        this.orderRepository = orderRepository;
    }
    @Override
    protected void mapSpecificFields(FilmWithDirectorsDTO source, Film destination) {
        if (!Objects.isNull(source.getFilmsIds())) {
            destination.setDirectors(new HashSet<>(directorRepository.findAllById(source.getFilmsIds())));
        }
        else {
            destination.setDirectors(Collections.emptySet());
        }
    }

    @Override
    protected void mapSpecificFields(Film source, FilmWithDirectorsDTO destination) {

    }

    @Override
    protected Set<Long> getIds(Film entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getId()) ? null : entity.getDirectors().stream().map(GenericModel::getId).collect(Collectors.toSet());
    }

    @Override
    protected void setupMapper() {
        modelMapper.createTypeMap(Film.class, FilmWithDirectorsDTO.class)
                .addMappings(m -> m.skip(FilmWithDirectorsDTO::setFilmsIds)).setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(FilmWithDirectorsDTO.class, Film.class)
                .addMappings(m -> m.skip(Film::setDirectors)).setPostConverter(toEntityConverter());}
}

