package FilmLibrary.mapper;
import jakarta.annotation.PostConstruct;
import myproject.filmoteka.dto.DirectorDTO;
import myproject.filmoteka.model.Director;
import myproject.filmoteka.model.GenericModel;
import myproject.filmoteka.repository.DirectorRepository;
import myproject.filmoteka.repository.FilmRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class DirectorMapper extends GenericMapper<Director, DirectorDTO> {

    private final DirectorRepository directorRepository;
    private final FilmRepository filmRepository;

    protected DirectorMapper(ModelMapper modelMapper, DirectorRepository directorRepository,
                             FilmRepository filmRepository) {
        super(modelMapper, Director.class, DirectorDTO.class);
        this.directorRepository = directorRepository;
        this.filmRepository = filmRepository;
    }


    @PostConstruct
    @Override
    public void setupMapper() {
        modelMapper.createTypeMap(Director.class, DirectorDTO.class)
                .addMappings(m -> m.skip(DirectorDTO::setDirectorsIds)).setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(DirectorDTO.class, Director.class)
                .addMappings(m -> m.skip(Director::setFilms)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(DirectorDTO source, Director destination) {
        if (!Objects.isNull(source.getDirectorsIds())) {
            destination.setFilms(new HashSet<>(filmRepository.findAllById(source.getDirectorsIds())));
        }
        else {
            destination.setFilms(Collections.emptySet());
        }
    }

    @Override
    protected void mapSpecificFields(Director source, DirectorDTO destination) {
        destination.setDirectorsIds(getIds(source));
    }


    @Override
    protected Set<Long> getIds(Director director) {
        return Objects.isNull(director) || Objects.isNull(director.getFilms()) ? Collections.emptySet() : director.getFilms().stream().map(GenericModel::getId).collect(Collectors.toSet());
    }
}
