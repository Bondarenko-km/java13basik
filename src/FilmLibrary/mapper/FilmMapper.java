package FilmLibrary.mapper;
import jakarta.annotation.PostConstruct;
import myproject.filmoteka.dto.FilmDTO;
import myproject.filmoteka.model.Film;
import myproject.filmoteka.model.GenericModel;
import myproject.filmoteka.model.Order;
import myproject.filmoteka.repository.DirectorRepository;
import myproject.filmoteka.repository.FilmRepository;
import myproject.filmoteka.repository.OrderRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class FilmMapper extends GenericMapper<Film, FilmDTO> {

    private final FilmRepository filmRepository;
    private final DirectorRepository directorRepository;
    private final OrderRepository orderRepository;

    public FilmMapper(ModelMapper modelMapper, FilmRepository filmRepository,
                      DirectorRepository directorRepository,
                      OrderRepository orderRepository) {
        super(modelMapper, Film.class, FilmDTO.class);
        this.filmRepository = filmRepository;
        this.directorRepository = directorRepository;
        this.orderRepository = orderRepository;
    }


    @PostConstruct
    @Override
    public void setupMapper() {
        modelMapper.createTypeMap(Film.class, FilmDTO.class)
                .addMappings(m -> m.skip(FilmDTO::setFilmsIds)).setPostConverter(toDTOConverter())
                .addMappings(m -> m.skip(FilmDTO::setOrdersIds)).setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(FilmDTO.class, Film.class)
                .addMappings(m -> m.skip(Film::setDirectors)).setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(Film::setOrders)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(FilmDTO source, Film destination) {
        if (!Objects.isNull(source.getFilmsIds())) {
            destination.setDirectors(new HashSet<>(directorRepository.findAllById(source.getFilmsIds())));
        }
        else {
            destination.setDirectors(Collections.emptySet());
        }
        if (!Objects.isNull(source.getOrdersIds())) {
            destination.setOrders(new HashSet<>(orderRepository.findAllById(source.getOrdersIds())));
        }
        else {
            destination.setOrders(Collections.emptySet());
        }
    }

    @Override
    protected void mapSpecificFields(Film source, FilmDTO destination) {
        destination.setFilmsIds(getIds(source));
        destination.setOrdersIds(getIds(source));
    }


    @Override
    protected Set<Long> getIds(Film book) {
        return Objects.isNull(book) || Objects.isNull(book.getDirectors()) ? Collections.emptySet() : book.getDirectors().stream().map(GenericModel::getId).collect(Collectors.toSet());
    }
}

