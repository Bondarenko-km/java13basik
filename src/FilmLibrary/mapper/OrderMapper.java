package FilmLibrary.mapper;
import jakarta.annotation.PostConstruct;
import myproject.filmoteka.dto.OrderDTO;
import myproject.filmoteka.model.Order;
import myproject.filmoteka.repository.FilmRepository;
import myproject.filmoteka.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.webjars.NotFoundException;

import java.util.Set;

@Component
public class OrderMapper extends GenericMapper<Order, OrderDTO> {

    private final UserRepository userRepository;
    private final FilmRepository filmRepository;

    public OrderMapper(ModelMapper modelMapper, UserRepository userRepository, FilmRepository filmRepository) {
        super(modelMapper,Order.class, OrderDTO.class);
        this.userRepository = userRepository;
        this.filmRepository = filmRepository;
    }

    @PostConstruct
    @Override
    public void setupMapper() {
        super.modelMapper.createTypeMap(Order.class, OrderDTO.class)
                .addMappings(m -> m.skip(OrderDTO::setUserId)).setPostConverter(toDTOConverter())
                .addMappings(m -> m.skip(OrderDTO::setFilmId)).setPostConverter(toDTOConverter());

        super.modelMapper.createTypeMap(OrderDTO.class, Order.class)
                .addMappings(m -> m.skip(Order::setUser)).setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(Order::setFilm)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(OrderDTO source, Order destination) {
        destination.setFilm(filmRepository.findById(source.getFilmId()).orElseThrow(() -> new NotFoundException("Фильм не найден")));
        destination.setUser(userRepository.findById(source.getUserId()).orElseThrow(() -> new NotFoundException("Пользователь не найден")));
    }

    @Override
    protected void mapSpecificFields(Order source, OrderDTO destination) {
        destination.setUserId(source.getUser().getId());
        destination.setFilmId(source.getFilm().getId());
    }

    @Override
    protected Set<Long> getIds(Order entity) {
        throw new UnsupportedOperationException("Метод недоступен");
    }
}

