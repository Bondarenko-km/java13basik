package FilmLibrary.repository;
import myproject.filmoteka.model.Order;
import org.springframework.stereotype.Repository;


@Repository
public interface OrderRepository extends GenericRepository<Order>{
}