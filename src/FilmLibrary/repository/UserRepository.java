package FilmLibrary.repository;
import myproject.filmoteka.model.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends GenericRepository<User> {
    User findUserByLogin(String username);

    User findUserByEmail(String email);

    User findUserByChangePasswordToken(String token);
}