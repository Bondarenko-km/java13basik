package FilmLibrary.repository;
import myproject.filmoteka.model.Director;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface DirectorRepository extends GenericRepository<Director> {

    Page<Director> findAllByDirectorFioContainsIgnoreCaseAndIsDeletedFalse(String fio,
                                                                           Pageable pageable);


    @Query(nativeQuery = true, value = """
        select d.* from directors d
        left join film_directors fd on d.id = fd.director_id
        left join films f on f.id = fd.film_id
        where d.directors_fio ilike '%' || :fio || '%'
        and f.title ilike '%' || :title || '%'
        """)

    Page<Director> searchDirectors(
            @Param(value = "fio") String fio,
            @Param(value = "title") String title,
            Pageable pageable);

    boolean checkDeletedForDeletion(final Long directorId);

}
