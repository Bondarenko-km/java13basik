package FilmLibrary.model;
public enum Genre {

    ACTION("Боевик"),
    COMEDY("Комедия"),
    DRAMA("Драма"),
    FANTASY("Фэнтези"),
    THRILLER("Триллер"),
    ROMANCE("Мелодрама");

    private final String genreOfFilm;

    Genre(String str) {
        this.genreOfFilm = str;
    }

    public String getGenreOfFilm() {
        return genreOfFilm;
    }
