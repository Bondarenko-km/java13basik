package FilmLibrary.test;
import myproject.filmoteka.dto.RoleDTO;
import myproject.filmoteka.dto.UserDTO;
import myproject.filmoteka.model.Role;
import myproject.filmoteka.model.User;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;

public interface UserTestData { UserDTO USER_DTO = new UserDTO("login",
        "password",
        "email",
        "birthDate",
        "firstName",
        "lastName",
        "middleName",
        "phone",
        "address",
        new RoleDTO(),
        "changePasswordToken",
        new HashSet<>()

);

    List<UserDTO> USER_DTO_LIST = List.of(USER_DTO);

    User USER = new User("login",
            "password",
            "email",
            LocalDate.now(),
            "firstName",
            "lastName",
            "middleName",
            "phone",
            "address",
            "changePasswordToken",
            new Role(),
            new HashSet<>()
    );

    List<User> USER_LIST = List.of(USER);


}

