package FilmLibrary.test.service;
import myproject.filmoteka.dto.UserDTO;
import myproject.filmoteka.expection.MyDeleteException;
import myproject.filmoteka.model.User;
import org.junit.jupiter.api.Test;

public class UserServiceTest extends GenericTest<User, UserDTO> {

    @Test
    @Override
    protected void getAll() {

    }

    @Test
    @Override
    protected void getOne() {

    }

    @Test
    @Override
    protected void create() {

    }

    @Test
    @Override
    protected void update() {

    }

    @Test
    @Override
    protected void delete() throws MyDeleteException {

    }

    @Test
    @Override
    protected void restore() {

    }

    @Test
    @Override
    protected void getAllNotDeleted() {

    }
}
