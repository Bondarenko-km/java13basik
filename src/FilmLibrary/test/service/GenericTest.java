package FilmLibrary.test.service;
import myproject.filmoteka.dto.GenericDTO;
import myproject.filmoteka.expection.MyDeleteException;
import myproject.filmoteka.mapper.GenericMapper;
import myproject.filmoteka.model.GenericModel;
import myproject.filmoteka.repository.GenericRepository;
import myproject.filmoteka.service.GenericService;
import myproject.filmoteka.service.userdetails.CustomUserDetails;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;

public abstract class GenericTest<E extends GenericModel, D extends GenericDTO> {
    protected GenericService<E, D> service;
    protected GenericRepository<E> repository;
    protected GenericMapper<E, D> mapper;

    @BeforeEach
    void init() {
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(CustomUserDetails
                .builder()
                .username("USER"),
                null,
                null);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
    protected abstract void getAll();

    protected abstract void getOne();

    protected abstract void create();

    protected abstract void update();

    protected abstract void delete() throws MyDeleteException;

    protected abstract void restore();

    protected abstract void getAllNotDeleted();

}

