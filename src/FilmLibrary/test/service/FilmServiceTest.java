package FilmLibrary.test.service;
import myproject.filmoteka.dto.DirectorDTO;
import myproject.filmoteka.dto.FilmDTO;
import myproject.filmoteka.expection.MyDeleteException;
import myproject.filmoteka.model.Film;
import org.junit.jupiter.api.Test;

public class FilmServiceTest extends GenericTest<Film, FilmDTO> {

    @Test
    @Override
    protected void getAll() {

    }

    @Test
    @Override
    protected void getOne() {

    }

    @Test
    @Override
    protected void create() {

    }

    @Test
    @Override
    protected void update() {
        DirectorDTO directorDTO;

    }

    @Test
    @Override
    protected void delete() throws MyDeleteException {

    }

    @Test
    @Override
    protected void restore() {

    }

    @Test
    @Override
    protected void getAllNotDeleted() {

    }
}

