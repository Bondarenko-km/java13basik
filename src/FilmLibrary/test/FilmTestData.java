package FilmLibrary.test;
import myproject.filmoteka.dto.FilmDTO;
import myproject.filmoteka.dto.FilmWithDirectorsDTO;
import myproject.filmoteka.model.Film;
import myproject.filmoteka.model.Genre;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public interface FilmTestData {
    FilmDTO FILM_DTO1 = new FilmDTO("Title1",
            "premierYear1",
            1,
            1,
            "country",
            " onlineCopyPath1",
            "publish1",
            "description1",
            Genre.DRAMA,
            new HashSet<>(),
            false);

    FilmDTO FILM_DTO2= new FilmDTO("Title2",
            "premierYear2",
            2,
            2,
            "country2",
            "onlineCopyPath2",
            "publish2",
            "description2",
            Genre.FANTASY,
            new HashSet<>(),
            false);

    List<FilmDTO> FILM_DTO_LIST = Arrays.asList(FILM_DTO1, FILM_DTO2);

    Film FILM_1 = new Film("Title1",
            LocalDate.now(),
            1,
            1,
            "publish1",
            "storagePlace1",
            "onlineCopyPath1",
            "description",
            Genre.DRAMA,
            new HashSet<>(),
            new HashSet<>());
    Film  FILM_2 = new Film("Title2",
            LocalDate.now(),
            2,
            2,
            "publish2",
            "storagePlace2",
            "onlineCopyPath2",
            "description2",
            Genre.FANTASY,
            new HashSet<>(),
            new HashSet<>());

    List<Film> FILM_LIST = Arrays.asList(FILM_1, FILM_2);

    Set<FilmDTO> DIRECTORS = new HashSet<>(FilmTestData.FILM_DTO_LIST);
    FilmWithDirectorsDTO FILM_WITH_DIRECTORS_DTO_1 = new FilmWithDirectorsDTO(FILM_1, DIRECTORS);
    FilmWithDirectorsDTO FILM_WITH_DIRECTORS_DTO_2 = new FilmWithDirectorsDTO(FILM_2, DIRECTORS);

    List<FilmWithDirectorsDTO> FILM_WITH_DIRECTORS_DTO_LIST = Arrays.asList(FILM_WITH_DIRECTORS_DTO_1, FILM_WITH_DIRECTORS_DTO_2);
}

