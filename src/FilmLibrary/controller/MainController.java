package FilmLibrary.controller;
import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Hidden
public class MainController {

    @RequestMapping("/")
    public String index() {
        return "index";
    }
}