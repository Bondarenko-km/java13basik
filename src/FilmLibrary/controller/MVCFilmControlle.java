package FilmLibrary.controller;
import io.swagger.v3.oas.annotations.Hidden;
import myproject.filmoteka.dto.*;
import myproject.filmoteka.service.DirectorService;
import myproject.filmoteka.service.FilmService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@Hidden
@RequestMapping("films")
public class MVCFilmController {
    private FilmService filmService;
    private DirectorService directorService;

    public MVCFilmController(FilmService filmService, DirectorService directorService) {
        this.filmService = filmService;
        this.directorService = directorService;
    }

    @GetMapping("")
    public String getAll(@RequestParam(value = "page", defaultValue = "1") int page,
                         @RequestParam(value = "size", defaultValue = "5") int pageSize,
                         Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.Direction.ASC, "title");
        Page<FilmWithDirectorsDTO> filmsDTOlist = filmService.getAllFilmsWithDirectors(pageRequest);
        model.addAttribute("films", filmsDTOlist);
        return "films/viewAllFilms";
    }


    @GetMapping("/{id}")
    public String getOne(@PathVariable Long id, Model model) {
        model.addAttribute("film", filmService.getFilmWithDirectors(id));
        return "films/viewFilm";
    }

    @GetMapping("/add")
    public String create() {
        return "films/addFilm";
    }


    @PostMapping("/add")
    public String create(@ModelAttribute("filmForm") FilmDTO filmDTO) {
        filmService.create(filmDTO);
        return "redirect:/films";
    }

    @GetMapping("/addDirector/{id}")
    public String addDirector(@PathVariable Long id, Model model) {
        model.addAttribute("film", filmService.getOne(id));
        model.addAttribute("allDirectors", directorService.listAll());
        return "films/addDirectorToFilm";
    }

    @PostMapping("/addDirector")
    public String addDirector(@ModelAttribute("idFilmAndDirectorDTO") IdFilmAndDirectorDTO idFilmAndDirectorDTO) {
        Long filmId = idFilmAndDirectorDTO.getFilmId();
        Long directorId = idFilmAndDirectorDTO.getDirectorId();
        FilmDTO filmDTO = filmService.getOne(filmId);
        DirectorDTO directorDTO = directorService.getOne(directorId);

        if (!filmDTO.getFilmsIds().contains(filmId)) {
            filmDTO.getFilmsIds().add(directorDTO.getId());
            filmService.update(filmDTO);
        }
        return "redirect:/films";
    }

    @PostMapping("/search")
    public String searchBooks(@ModelAttribute("filmSearchForm") FilmSearchDTO filmSearchDTO,
                              Model model,
                              @RequestParam(value = "page", defaultValue = "1") int page,
                              @RequestParam(value = "pageSize", defaultValue = "5") int pageSize) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "title"));
        model.addAttribute("films", filmService.findFilms(filmSearchDTO, pageRequest));
        return "films/viewAllFilms";
    }
}

