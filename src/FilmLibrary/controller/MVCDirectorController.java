package FilmLibrary.controller;
import io.swagger.v3.oas.annotations.Hidden;
import myproject.filmoteka.dto.DirectorDTO;
import myproject.filmoteka.dto.DirectorSearchDTO;
import myproject.filmoteka.dto.DirectorWithFilmsDTO;
import myproject.filmoteka.dto.IdFilmAndDirectorDTO;
import myproject.filmoteka.service.DirectorService;
import myproject.filmoteka.service.FilmService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@Hidden
@RequestMapping("directors")
public class MVCDirectorController {

    private DirectorService directorService;
    private FilmService filmService;

    public MVCDirectorController(DirectorService directorService, FilmService filmService) {
        this.directorService = directorService;
        this.filmService = filmService;
    }

    @GetMapping("")
    public String getAll(@RequestParam(value = "page", defaultValue = "1") int page,
                         @RequestParam(value = "size", defaultValue = "5") int pageSize,
                         Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.Direction.ASC, "directorsFio");
        Page<DirectorWithFilmsDTO> directorDTOList = directorService.getAllDirectorsWithFilms(pageRequest);
        model.addAttribute("directors", directorDTOList);
        return "directors/viewAllDirectors";
    }


    @GetMapping("/{id}")
    public String getOne(@PathVariable Long id, Model model) {
        model.addAttribute("director", directorService.getDirectorWithFilms(id));
        return "directors/viewDirector";
    }


    @GetMapping("/add")
    public String create() {
        return "directors/addDirector";
    }

    @PostMapping("/add")
    public String create(@ModelAttribute("directorForm") DirectorDTO directorDTO) {
        directorService.create(directorDTO);
        return "redirect:/directors";
    }

    @GetMapping("/addFilm/{id}")
    public String addFilm(@PathVariable Long id, Model model) {
        model.addAttribute("director", directorService.getOne(id));
        model.addAttribute("films", filmService.listAll());
        return "directors/addFilmToDirector";
    }


    @PostMapping("/addFilm")
    public String addFilm(@ModelAttribute("idFilmAndDirectorDTO") IdFilmAndDirectorDTO idFilmAndDirectorDTO) {
        Long directorId = idFilmAndDirectorDTO.getDirectorId();
        Long filmId = idFilmAndDirectorDTO.getFilmId();
        DirectorDTO directorDTO = directorService.getOne(directorId);
        if (!directorDTO.getDirectorsIds().contains(filmId)) {
            directorDTO.getDirectorsIds().add(filmId);
            directorService.update(directorDTO);
        }
        return "redirect:/directors";
    }


    @PostMapping("/search")
    public String searchBooks(@ModelAttribute("directorSearchForm") DirectorSearchDTO directorSearchDTO,
                              Model model,
                              @RequestParam(value = "page", defaultValue = "1") int page,
                              @RequestParam(value = "pageSize", defaultValue = "5") int pageSize) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "directors_fio"));
        model.addAttribute("directors", directorService.findDirectors(directorSearchDTO, pageRequest));
        return "directors/viewAllDirectors";
    }
}
