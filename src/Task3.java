import java.util.Arrays;
import java.util.Scanner;
//На вход подается число N — длина массива. Затем передается массив
//целых чисел (ai) из N элементов, отсортированный по возрастанию.
//Необходимо создать массив, полученный из исходного возведением в квадрат
//каждого элемента, упорядочить элементы по возрастанию и вывести их на
//экран.
public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
        }
        sum(arr);
    }
    public static void sum(int[] arr) {
        int[] newArr = new int[arr.length];
        int pos = 0;
        for (int element : arr) {
            newArr[pos] = (int) Math.pow(element, 2);
            pos++;
        }
        Arrays.sort(newArr);
        for (int elem: newArr) {
            System.out.print(elem + " ");
        }
    }
}