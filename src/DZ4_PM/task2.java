package DZ4_PM;
/*
     На вход подается список целых чисел. Необходимо вывести результат перемножения
этих чисел.
Например, если на вход передали List.of(1, 2, 3, 4, 5), то результатом должно быть число
120 (т.к. 1 * 2 * 3 * 4 * 5 = 120).
 */
import java.util.List;
public class task2 {
    public static void main(String[] args) {
        int mult = List.of(1,2,3,4,5).stream()
                .mapToInt(a -> a)
                .reduce(1, (a, b) -> a * b);
        System.out.println(mult);


    }
}

