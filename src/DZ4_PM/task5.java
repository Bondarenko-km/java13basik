package DZ4_PM;
/*
    На вход подается список непустых строк. Необходимо привести все символы строк к
верхнему регистру и вывести их, разделяя запятой.
Например, для List.of("abc", "def", "qqq") результат будет ABC, DEF, QQQ.
 */
import java.util.List;
import java.util.stream.Collectors;
public class task5 {
    public static void main(String[] args) {
        List<String> asList = List.of("abc", "def", "qqq");
        List<String> collect = asList.stream().map(String::toUpperCase).collect(Collectors.toList());
        System.out.println(collect);
    }
}

