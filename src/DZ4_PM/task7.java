package DZ4_PM;
/*
На вход подается две строки.
Необходимо определить, можно ли уравнять эти две строки, применив только одну из трех
возможных операций:
1. Добавить символ
2. Удалить символ
3. Заменить символ
 */
import java.util.Scanner;
public class task7 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String s1 = scan.nextLine();
        String s2 = scan.nextLine();
        System.out.println(checkCompare(s1, s2));
    }
    public static boolean checkCompare(String s1, String s2) {
        if (s1.length() == s2.length() - 1) {
            int count = 0;
            for (int i = 0; i < s1.length(); i++) {
                if (s2.contains(String.valueOf(s1.charAt(i)))) {
                    count++;
                }
            }
            return count == s1.length();
        }
        if (s1.length() == s2.length()) {
            int count = 0;
            for (int i = 0; i < s1.length(); i++) {
                if(s1.charAt(i) == s2.charAt(i)) {
                    count++;
                }
            }
            return count == s1.length() - 1;
        }
        if (s1.length() == s2.length() + 1) {
            int count = 0;
            for (int i = 0; i < s2.length(); i++) {
                if (s1.contains(String.valueOf(s2.charAt(i)))) {
                    count++;
                }
            }
            return count == s2.length();
        }
        return false;
    }
}
