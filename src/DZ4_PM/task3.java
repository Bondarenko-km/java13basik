package DZ4_PM;
/*
   На вход подается список строк. Необходимо вывести количество непустых строк в
списке.
Например, для List.of("abc", "", "", "def", "qqq") результат равен 3.
 */
import java.util.List;
public class task3 {
    public static void main(String[] args) {
        long count = List.of("abc", "", "", "def", "qqq").stream()
                .filter(str -> str.length() > 0)
                .count();
        System.out.println(count);


    }
}

