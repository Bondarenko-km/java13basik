package DZ4_PM;
/*
    Посчитать сумму четных чисел в промежутке от 1 до 100 включительно и вывести ее на
экран
 */
import java.util.stream.IntStream;
public class task1 {
    public static void main(String[] args) {
        int sum = IntStream.rangeClosed(1, 100).filter(digit -> digit % 2 ==0).sum();
        System.out.println(sum);
    }
}
