package DZ4_PM;
/*
      Дан Set<Set<Integer>>. Необходимо перевести его в Set<Integer>
 */
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;
public class task6 {
    public static void main(String[] args) {
        Set<Set<Integer>> setSet = Set.of(Set.of(1, 2, 3));

        Set<Integer> set = setSet.stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());
        System.out.println(set);
    }
}

