package DZ4_PM;
/*
     На вход подается список вещественных чисел. Необходимо отсортировать их по
убыванию
 */
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
public class task4 {
    public static void main(String[] args) {
        List<Double> list = Arrays.asList(10.0, -3.0, 1.0,  4.0, -2.0,  8.0 );
        List<Double> sortedList = list.stream()
                .sorted(Collections.reverseOrder())
                .collect(Collectors.toList());
        System.out.println(sortedList);


    }
}
