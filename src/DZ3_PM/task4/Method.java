package DZ3_PM.task4;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
public class Method {
    public static Set<Class<?>> getAllInterfaces(Class<?> clazz) {
        Set<Class<?>> allInterfaces = new HashSet<>();
        while(clazz != Object.class) {
            Set<Class<?>> interfaces = new HashSet<>(List.of(clazz.getInterfaces()));
            for (Class<?> inter : interfaces) {
                while(inter != null) {
                    interfaces.addAll(List.of(inter.getInterfaces()));
                    inter = inter.getSuperclass();
                }
            }
            allInterfaces.addAll(interfaces);
            clazz = clazz.getSuperclass();
        }
        return allInterfaces;
    }
}

