package DZ3_PM.task3;
//Есть класс APrinter:
//public class APrinter {
//public void print(int a) {
//System.out.println(a);
//}
//}
//Задача: с помощью рефлексии вызвать метод print() и обработать все
//возможные ошибки (в качестве аргумента передавать любое подходящее
//число). При “ловле” исключений выводить на экран краткое описание ошибки.
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
public class main {
    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        Class<APrinter> clazz = APrinter.class;

        try {
            Method method = clazz.getMethod("print", int.class);
            method.setAccessible(true);
            method.invoke(clazz.getConstructor().newInstance(), 3);
        }
        catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | IllegalArgumentException ex ) {
            System.out.println("Произошла ошибка");
        }
    }
}
