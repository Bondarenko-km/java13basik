package DZ3_PM.task2;
//Написать метод, который рефлексивно проверит наличие аннотации @IsLike на
//любом переданном классе и выведет значение, хранящееся в аннотации, на
//экран.
import DZ3_PM.task1.IsLike;
public class main {
    public static void main(String[] args) {
        Class<WithAnnotation> clazz1 = WithAnnotation.class;
        checkAnnotation(clazz1);

        Class<WithOutAnnotation> clazz2 = WithOutAnnotation.class;
        checkAnnotation(clazz2);
    }
    public static void checkAnnotation(Class<?> clazz) {
        if (clazz.isAnnotationPresent(IsLike.class)) {
            System.out.println(clazz.getAnnotation(IsLike.class));
        }
        else {
            System.out.println("Такой аннотации нет");
        }
    }
}

