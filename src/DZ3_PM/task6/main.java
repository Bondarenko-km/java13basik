package DZ3_PM.task6;
//Дана строка, состоящая из символов “(“, “)”, “{”, “}”, “[“, “]”
//Необходимо написать метод, принимающий эту строку и выводящий результат,
//является ли она правильной скобочной последовательностью или нет.
//Условия для правильной скобочной последовательности те, же что и в задаче 1,
//но дополнительно:
//● Открывающие скобки должны быть закрыты однотипными
//закрывающими скобками.
//● Каждой закрывающей скобке соответствует открывающая скобка того же
//типа.
import java.util.Deque;
import java.util.LinkedList;
public class main {
    public static void main(String[] args) {
        System.out.println(checkString("{()[]()}"));
    }
    public static boolean checkString(String str) {
        if (str.length() == 0) {
            return true;
        }
        Deque<Character> deque = new LinkedList<>();
        char[] array = str.toCharArray();
        for (char elem : array) {
            if (elem == '(' || elem == '[' || elem == '{') {
                deque.addFirst(elem);
            }
            else {
                if ((!deque.isEmpty() && ((deque.getFirst() == '{' && elem == '}') || (deque.getFirst() == '[' && elem == ']') || (deque.getFirst() == '(' && elem == ')')))) {
                    deque.removeFirst();
                }
                else {
                    return false;
                }
            }
        }
        return deque.isEmpty();
    }
}
