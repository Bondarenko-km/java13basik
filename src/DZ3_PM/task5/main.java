package DZ3_PM.task5;
//Дана строка, состоящая из символов “(“ и “)”
//Необходимо написать метод, принимающий эту строку и выводящий результат,
//является ли она правильной скобочной последовательностью или нет.
//Строка является правильной скобочной последовательностью, если:
//● Пустая строка является правильной скобочной последовательностью.
//● Пусть S — правильная скобочная последовательность, тогда (S) есть
//правильная скобочная последовательность.
//● Пусть S1, S2 — правильные скобочные последовательности, тогда S1S2
//есть правильная скобочная последовательность.
import java.util.Scanner;
public class main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String s = scan.nextLine();
        checkString(s);
    }
    public static void checkString(String s) {
        int count = 0;
        if (s.length() == 0) {
            System.out.println(true);
        }
        else {
            for (int i = 0; i < s.length(); i++) {
                if (count < 0) {
                    break;
                }
                if (s.charAt(i) == '(') {
                    count++;
                }
                else if (s.charAt(i) == ')') {
                    count--;
                }
            }
            if (count == 0) {
                System.out.println(true);
            }
            else {
                System.out.println(false);
            }

        }
    }
}
