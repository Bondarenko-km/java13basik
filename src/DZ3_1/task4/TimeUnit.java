package DZ3_1.task4;

public class TimeUnit {
    private int hours;
    private int minutes;
    private int seconds;
    public TimeUnit() {
    }
    /**
     * Конструктор, который создаёт объект с 3 параметрами
     * @param hours Часы
     * @param minutes Минуты
     * @param seconds Секунды
     */
    public TimeUnit(int hours, int minutes, int seconds) {
        if (hours >= 0 && hours < 24 ) {
            this.hours = hours;
        }
        else {
            this.hours = hours - 24;
        }
        if (minutes >=0 && minutes < 61) {
            this.minutes = minutes;
        }
        else {
            this.minutes = minutes - 60;
        }
        if (seconds >= 0 && seconds < 61) {
            this.seconds = seconds;
        }
        else {
            this.seconds = seconds - 60;
        }
    }

    /**
     * Конструктор, который создаёт объект с 2 параметрами: часы, минуты (значение поля данных seconds = 0)
     * @param hours Часы
     * @param minutes Минуты
     */
    public TimeUnit(int hours, int minutes) {
        if (hours >= 0 && hours < 24 ) {
            this.hours = hours;
        }
        else {
            System.out.println("Некорректное количество часов");
        }
        if (minutes >=0 && minutes < 61) {
            this.minutes = minutes;
        }
        else {
            System.out.println("Некорректное количество минут");
        }
        this.seconds = 0;
    }
    /**
     * Конструктор, который создаёт объект с 1-им параметром: часы (значения полей данных minutes и seconds равны 0)
     * @param hours Часы
     */
    public TimeUnit(int hours) {
        if (hours >= 0 && hours < 24 ) {
            this.hours = hours;
        }
        else {
            System.out.println("Некорректное количество часов");
        }
        this.minutes = 0;
        this.seconds = 0;
    }
    public int getHours() {
        return this.hours;
    }
    public int getMinutes() {
        return this.minutes;
    }
    public int getSeconds() {
        return this.seconds;
    }
    /**
     * Метод, выводящий на экран установленное в классе время в формате hh:mm:ss
     */
    public void getTime() {
        System.out.printf("%02d:%02d:%02d" + "\n", getHours(), getMinutes(), getSeconds());
    }
    /**
     * Метод, выводящий на экран установленное в классе время в 12 - часовом формате
     */
    public void getAmPmTime() {
        if (getHours() > 12) {
            System.out.printf("%02d:%02d:%02d pm" + "\n",(getHours() - 12), getMinutes(), getSeconds());
        }
        else {
            System.out.printf("%02d:%02d:%02d am" + "\n", getHours(), getMinutes(), getSeconds());
        }
    }
    /**
     * Метод, прибавляющий переданное время к установленному
     * @param hours Часы
     * @param minutes Минуты
     * @param seconds Секунды
     */
    public void setTime(int hours, int minutes, int seconds) {
        this.hours = getHours() + hours;
        if (this.hours > 23) {
            this.hours = this.hours - 24;
        }
        this.minutes = getMinutes() + minutes;
        if (this.minutes > 60) {
            this.minutes = this.minutes - 60;
            this.hours = this.hours + 1;
        }
        this.seconds = getSeconds() + seconds;
        if (this.seconds > 60) {
            this.seconds = this.seconds - 60;
            this.minutes = this.minutes + 1;
        }
    }
}

