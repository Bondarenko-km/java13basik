package DZ3_1.task4;
//Необходимо реализовать класс TimeUnit с функционалом, описанным ниже
//(необходимые поля продумать самостоятельно). Обязательно должны быть
//реализованы валидации на входные параметры.
//Конструкторы:
// Возможность создать TimeUnit, задав часы, минуты и секунды.
//Возможность создать TimeUnit, задав часы и минуты. Секунды тогда
//должны проставиться нулевыми.
//Возможность создать TimeUnit, задав часы. Минуты и секунды тогда
//должны проставиться нулевыми.
//Публичные методы:
// Вывести на экран установленное в классе время в формате hh:mm:ss
// Вывести на экран установленное в классе время в 12-часовом формате
//(используя hh:mm:ss am/pm)
// Метод, который прибавляет переданное время к установленному в
//TimeUnit (на вход передаются только часы, минуты и секунды).
public class main {
    public static void main(String[] args) {
        TimeUnit time1 = new TimeUnit(22,52,59);
        TimeUnit time2 =  new TimeUnit(8);

        time1.getTime();
        time1.setTime(3,15,25);
        time1.getTime();

        time2.getTime();
        time2.getAmPmTime();
        time2.setTime(10, 25, 34);
        time2.getAmPmTime();
    }
}
