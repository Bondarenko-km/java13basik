package DZ3_1.task6;
//Необходимо реализовать класс AmazingString, который хранит внутри себя
//строку как массив char и предоставляет следующий функционал:
//Конструкторы:
//Создание AmazingString, принимая на вход массив char
//Создание AmazingString, принимая на вход String
//Публичные методы (названия методов, входные и выходные параметры
//продумать самостоятельно). Все методы ниже нужно реализовать “руками”, т.е.
//не прибегая к переводу массива char в String и без использования стандартных
//методов класса String.
// Вернуть i-ый символ строки
// Вернуть длину строки
// Вывести строку на экран
// Проверить, есть ли переданная подстрока в AmazingString (на вход
//подается массив char). Вернуть true, если найдена и false иначе
// Проверить, есть ли переданная подстрока в AmazingString (на вход
//подается String). Вернуть true, если найдена и false иначе
// Удалить из строки AmazingString ведущие пробельные символы, если
//они есть
// Развернуть строку (первый символ должен стать последним, а
//последний первым и т.д.)
public class main {
    public static void main(String[] args) {

        char[] arr = {'w', 'e', 'l', 'c', 'o', 'm'};
        char[] substring = {'c', 'a', 't'};
        AmazingString test = new AmazingString(arr);
        System.out.println(test.getChar(2)); // Проверка работоспособности метода, который выводит элемент по заданному индексу
        System.out.println(test.getLengthString()); // Проверка работоспособности метода, который выводит длину строки, являющаяся массивом символов
        test.getOurString(); // Проверка работоспособности метода, который выводит  строку, являющаяся массивом символов
        System.out.println(test.foundSubstringFromArray(substring)); // Проверка работоспособности метода, который проверяет содержится ли переданная подстрока(которая подаётся через массив char) в AmazingString
        // и в зависимости от ответа выводит true / false
        System.out.println(test.foundSubstringFromString("oka")); // Проверка работоспособности метода, который проверяет содержится ли переданная подстрока(которая подаётся через массив char) в AmazingString
        // и в зависимости от ответа выводит true / false

        AmazingString test1 = new AmazingString("  dog");
        test1.getOurString();
        test1.deleteSpace();
        test1.getOurString();
        test.getOurString();
        test.reverseString();
        test.getOurString();
    }
}
