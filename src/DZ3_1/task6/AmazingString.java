package DZ3_1.task6;

public class AmazingString {
    private char[] arr;
    public AmazingString(char[] arr) {
        this.arr = arr;
    }
    public AmazingString(String string) {
        this.arr = string.toCharArray();
    }
    public char getChar(int ind) {
        char ch = ' ';
        for (int i = 0; i < arr.length; i++) {
            if (i == ind) {
                ch = arr[i];
            }
        }
        return ch;
    }
    /**
     * Метод выводит длину строки, которая является массивом символов
     * @return
     */
    public int getLengthString() {
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            count++;
        }
        return count;
    }
    public void getOurString() {
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i]);
        }
        System.out.println();
    }
    public boolean foundSubstringFromArray(char[] substring) {
        int length = substring.length;
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < length; j++) {
                if (substring[j] == arr[i]){
                    count++;
                    i++;
                }
            }
        }
        if (count == length){
            return true;
        }
        else {
            return false;
        }
    }
    public boolean foundSubstringFromString(String string) {
        int length = 0;
        int count = 0;

        for (int i = 0; i < string.length(); i++) { // Цикл для нахождения длины строки без использования метода length()
            length++;
        }
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < length; j++) {
                if (string.charAt(j) == arr[i]) {
                    count++;
                    i++;
                }
            }
        }
        if (count == length) {
            return true;
        }
        else {
            return false;
        }
    }
    /**
     * Метод, который удаляет пробелы в строке
     */
    public void deleteSpace() {
        int countOfNotSpace = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] != ' ') {
                countOfNotSpace++;
            }
        }
        char[] withoutSpace = new char[countOfNotSpace];

        for (int i = 0, j = 0; i < arr.length && j < withoutSpace.length; i++) {
            if (arr[i] != ' ') {
                withoutSpace[j++] = arr[i];
            }
        }
        arr = withoutSpace;
    }
    /**
     * Метод, разворачивающий строку
     */
    public  void reverseString() {
        char[] reverseArray = new char[arr.length];
        for (int i = 0, j = arr.length - 1; i < arr.length && j >= 0; i++, j--){
            reverseArray[i] = arr[j];
        }
        arr = reverseArray;
    }
}
