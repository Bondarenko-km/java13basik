package DZ3_1.task1;
public class Cat {
    private void sleep() {
        System.out.println("Sleep");
    }
    private void meow() {
        System.out.println("Meow");
    }
    private void eat() {
        System.out.println("Eat");
    }
    public void status() {
        double random = (int) (1 + Math.random() * 3);
        if (random == 1) {
            sleep();
        } else if (random == 2) {
            meow();
        } else if (random == 3) {
            eat();
        }
    }
}
