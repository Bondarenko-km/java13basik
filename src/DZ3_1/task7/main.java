package DZ3_1.task7;
//Реализовать класс TriangleChecker, статический метод которого принимает три
//длины сторон треугольника и возвращает true, если возможно составить из них
//треугольник, иначе false. Входные длины сторон треугольника — числа типа.
//double. Придумать и написать в методе main несколько тестов для проверки
//работоспособности класса (минимум один тест на результат true и один на
//результат false)
public class main {
    public static void main(String[] args) {
        // Тест для проверки работоспособности метода, который выводит true
        System.out.println(TriangleChecker.isTriangle(1, 2, 2));
        // Тест для проверки работоспособности метода, который выводит false
        System.out.println(TriangleChecker.isTriangle(1, 2, 5));
        // Ещё одна проверка, которая выводит true
        System.out.println(TriangleChecker.isTriangle(5, 5, 5));
        // Ещё одна проверка, которая выводит false
        System.out.println(TriangleChecker.isTriangle(4, 5, 15));
    }
}
