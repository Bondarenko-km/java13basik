package DZ3_1.task7;

public class TriangleChecker {
    public static boolean isTriangle(double firstSide, double secondSide, double thirdSide) {
        return firstSide + secondSide > thirdSide && firstSide + thirdSide > secondSide && secondSide + thirdSide > firstSide;
    }
}
