package DZ3_1.task8;

public class Atm {
    private double currencyRateOfRoublesForDollars; // Курс покупки 1 доллара за рубли
    private double currencyRateOfDollarsForRoubles; // Курс покупки рублей за 1 доллар
    private double amountOfRoubles; // Сумма рублей
    private double amountOfDollars; // Сумма долларов
    private static int countOfInstance; // Счётчик количества созданных экземпляров класса
    public Atm(double currencyRateOfRoublesForDollars, double currencyRateOfDollarsForRoubles) {
        this.currencyRateOfRoublesForDollars = currencyRateOfRoublesForDollars;
        this.currencyRateOfDollarsForRoubles = currencyRateOfDollarsForRoubles;
        countOfInstance++;
    }
    public static int getCountOfInstance() {
        return countOfInstance;
    }
    public double getCurrencyRateOfRoublesForDollars() { // метод, который выводит курс покупки 1 доллара в рублях
        return currencyRateOfRoublesForDollars;
    }
    public double getCurrencyRateOfDollars() { // метод, который выводит курс продажи 1 доллара в рублях
        return currencyRateOfDollarsForRoubles;
    }
    /**
     * Метод, переводящий сумму рублей в сумму долларов по указанному курсу
     * @param amountOfRoubles Общая сумма рублей, которая будет переведена в доллары
     * @return
     */
    public double convertFromRoublesToDollars(double amountOfRoubles) {
        return this.amountOfDollars = amountOfRoubles / currencyRateOfRoublesForDollars;
    }
    /**
     * Метод, переводящий сумму долларов в рубли по указанному курсу в конструкторе
     * @param amountOfDollars Общая сумма долларов, которая будет переведена в рубли
     * @return
     */
    public double convertFromDollarsToRoubles(double amountOfDollars) {
        return this.amountOfRoubles = amountOfDollars * currencyRateOfDollarsForRoubles;
    }
}

