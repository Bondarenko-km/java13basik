package DZ3_1.task2;
import java.util.Arrays;
//Необходимо реализовать класс Student.
//У класса должны быть следующие приватные поля:
// String name — имя студента
// String surname — фамилия студента
// int[] grades — последние 10 оценок студента. Их может быть меньше, но
//не может быть больше 10.
//И следующие публичные методы:
//геттер/сеттер для name
// геттер/сеттер для surname
// геттер/сеттер для grades
// метод, добавляющий новую оценку в grades. Самая первая оценка
//должна быть удалена, новая должна сохраниться в конце массива (т.е.
//массив должен сдвинуться на 1 влево).
// метод, возвращающий средний балл студента (рассчитывается как
//среднее арифметическое от всех оценок в массиве grades
public class main {
    public static void main(String[] args) {
        Student student =  new Student();
        student.setName("Петр");
        student.setSurname("Петров");
        int[] scoreStudent = {3,3,4,4,4,5,3,4,5};
        student.setGrades(scoreStudent);
        student.setScore(scoreStudent, 4);
        System.out.println("Имя студента " + student.getName());
        System.out.println("Фамилия студента " + student.getSurname());
        System.out.println("Средний балл студента " + (int)student.getAverageStudentScore());
        // проверка, что оценки сдвигаются
        System.out.println("Последние оценки студента: " + Arrays.toString(student.getGrades()));
    }
}
