package DZ3_1.task5;
//Необходимо реализовать класс DayOfWeek для хранения порядкового номера
//дня недели (byte) и названия дня недели (String).
//Затем в отдельном классе в методе main создать массив объектов DayOfWeek
//длины 7. Заполнить его соответствующими значениями (от 1 Monday до 7
//Sunday) и вывести значения массива объектов DayOfWeek на экран.
//Пример вывода:
//1 Monday
//2 Tuesday
//…
//7 Sunday
public class main {
    public static void main(String[] args) {
        DayOfWeek[] days = new DayOfWeek[7];

        for (int i = 0; i < days.length; i++) {
            days[i] = new DayOfWeek();
            days[i].setNumberOfDay((byte)(i + 1));
        }
        days[0].setName("Monday");
        days[1].setName("Tuesday");
        days[2].setName("Wednesday");
        days[3].setName("Thursday");
        days[4].setName("Friday");
        days[5].setName("Saturday");
        days[6].setName("Sunday");

        for (int i = 0; i < days.length; i++) {
            System.out.println(days[i].getNumberOfDay() + " " + days[i].getName());
        }
    }
}
