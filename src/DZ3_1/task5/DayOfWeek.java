package DZ3_1.task5;

public class DayOfWeek {
    private byte numberOfDay; // Порядковый номер для недели
    private String name; // Название дня недели
    public byte getNumberOfDay() {
        return numberOfDay;
    }
    public void setNumberOfDay(byte numberOfDay) {
        this.numberOfDay = numberOfDay;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
}

