package DZ3_1.task3;
//Необходимо реализовать класс StudentService.
//У класса должны быть реализованы следующие публичные методы:
// bestStudent() — принимает массив студентов (класс Student из
//предыдущего задания), возвращает лучшего студента (т.е. который
//имеет самый высокий средний балл). Если таких несколько — вывести
//любого.
// sortBySurname() — принимает массив студентов (класс Student из
//предыдущего задания) и сортирует его по фамилии.

import DZ3_1.task2.Student;

public class main {
    public static void main(String[] args) {
        Student[] students = new Student[3];
        students[0] = new Student();
        students[0].setName("Егор");
        students[0].setSurname("Егоров");
        int[] arr1 = {2, 3, 5, 5, 5, 4, 3, 3, 3, 2};
        students[0].setGrades(arr1);

        students[1] = new Student();
        students[1].setName("Петр");
        students[1].setSurname("Петров");
        int[] arr2 = {3, 3, 4, 4, 4, 5, 3, 4, 5};
        students[1].setGrades(arr2);

        students[2] = new Student();
        students[2].setName("Иван");
        students[2].setSurname("Иванов");
        int[] arr3 = {5, 5, 5, 5, 4, 4, 4, 4, 4, 4};
        students[2].setGrades(arr3);

        StudentService list = new StudentService();
        System.out.println("Лучший студент " + list.bestStudent(students).getSurname() +
                " " + list.bestStudent(students).getName());

        list.sortBySurname(students);
        for (int i = 0; i < students.length; i++) {
            System.out.println(students[i].getSurname());
        }
    }
}
