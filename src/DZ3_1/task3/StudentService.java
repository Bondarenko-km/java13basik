package DZ3_1.task3;

import DZ3_1.task2.Student;

import java.util.Arrays;

public class StudentService {
    public Student bestStudent(Student[] students) {
        double maxAverageScore = students[0].getAverageStudentScore();
        int indexBestStudents = 0;

        for (int i = 0; i < students.length; i++) {
            if (students[i].getAverageStudentScore() > maxAverageScore) {
                maxAverageScore = students[i].getAverageStudentScore();
                indexBestStudents = i;
            }
        }
        return students[indexBestStudents];
    }
    public void sortBySurname(Student[] students) {
        String[] surnameOfStudents = new String[students.length]; // Создаём массив для внесения туда фамилий студентов
        for (int i = 0; i < students.length; i++) {
            surnameOfStudents[i] = students[i].getSurname();
        }
        Arrays.sort(surnameOfStudents);

        // В циклах проверяем совпадение фамилий и в случае совпадения меняем студентов
        for (int i = 0; i < students.length; i++) {
            for (int j = 0; j < students.length; j++) {
                if (surnameOfStudents[i].equals(students[j].getSurname())) {
                    Student[] temp = {students[i]}; // Массив для хранения объекта типа Student
                    students[i] = students[j];
                    students[j] = temp[0];
                }
            }
        }
    }
}

