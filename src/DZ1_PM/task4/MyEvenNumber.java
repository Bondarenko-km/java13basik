package DZ1_PM.task4;

public class MyEvenNumber {
    private int n;

    public MyEvenNumber(int n) throws MyPrimeNumber {
        if (n % 2 == 0) {
            this.n = n;
        }
        else {
            throw new MyPrimeNumber("Вы ввели нечётное число: " + n);
        }
    }
    public int getN() {
        return n;
    }
}
