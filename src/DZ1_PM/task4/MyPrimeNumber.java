package DZ1_PM.task4;

public class MyPrimeNumber extends Exception {

    public MyPrimeNumber() {
        super();
    }

    public MyPrimeNumber(String message) {
        super(message);
    }
}
