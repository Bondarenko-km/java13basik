package DZ1_PM.task8;
//На вход подается число n, массив целых чисел отсортированных по
//возрастанию длины n и число p. Необходимо найти индекс элемента массива
//равного p. Все числа в массиве уникальны. Если искомый элемент не найден,
//вывести -1.
import java.util.Scanner;
public class main {
    public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    int n = scan.nextInt();
    int[] arr = new int[n];

    for (int i = 0; i < arr.length; i++) {
        arr[i] = scan.nextInt();
    }
    int p = scan.nextInt();

    System.out.println(searchKey(p,arr,0, arr.length - 1));
}
        public static int searchKey(int key, int[] array, int lowIndex, int highIndex) {
            while (lowIndex <= highIndex) {
                int midIndex = (highIndex + lowIndex) / 2;
                if (array[midIndex] < key) {
                    lowIndex = midIndex + 1;
                } else if (array[midIndex] > key) {
                    highIndex = midIndex - 1;
                } else if (array[midIndex] == key) {
                    return midIndex;
                }
            }
            return -1;
        }
}
