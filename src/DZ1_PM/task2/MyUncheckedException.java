package DZ1_PM.task2;
//Создать собственное исключение MyUncheckedException, являющееся непроверяемым.

class MyUncheckedException extends RuntimeException {

    public MyUncheckedException() {
        super();
    }

    public MyUncheckedException(String message) {
        super(message);
    }

}
