package DZ1_PM.task7;
//На вход подается число n и массив целых чисел длины n. Вывести два максимальных числа в этой последовательности.
import java.util.Arrays;
import java.util.Scanner;
public class main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int[] array = new int[n];

        for (int i = 0; i < array.length; i++) {
            array[i] = scan.nextInt();
        }

        Arrays.sort(array);
        System.out.println(array[array.length - 1] + " " + array[array.length - 2]);
    }
}

