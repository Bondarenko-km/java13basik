package DZ1_PM.task6;

public class InvalidHeightException extends Exception {

    public InvalidHeightException() {
        super("Неверно указан рост. Рост должен быть положительным числом.");
    }
    public InvalidHeightException(String message) {
        super(message);
    }
}

