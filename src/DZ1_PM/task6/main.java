package DZ1_PM.task6;

import javax.naming.InvalidNameException;

//Фронт со своей стороны не сделал обработку входных данных анкеты! Петя
//очень зол и ему придется написать свои проверки, а также кидать исключения,
//если проверка провалилась. Помогите Пете написать класс FormValidator со
//статическими методами проверки. На вход всем методам подается String str.
//a. public void checkName(String str) — длина имени должна быть от 2 до 20 символов, первая буква заглавная.
//b. public void checkBirthdate(String str) — дата рождения должна быть не раньше 01.01.1900 и не позже текущей даты.
//c. public void checkGender(String str) — пол должен корректно матчится в enum Gender, хранящий Male и Female значения.
//d. public void checkHeight(String str) — рост должен быть положительным числом и корректно конвертироваться в double.
public class main {
    public static void main(String[] args) {
        try {
            FormValidator.checkName("Egor");
        } catch(InvalidNameException ex) {
            System.out.println(ex.getMessage());
        }

        try {
            FormValidator.checkBirthDate("31.12.1900");
        } catch (InvalidDateException ex) {
            System.out.println(ex.getMessage());
        }

        try {
            FormValidator.checkGender("MALE");
        } catch (InvalidGenderException ex) {
            System.out.println(ex.getMessage());
        }

        try {
            FormValidator.checkHeight("195");
        } catch (InvalidHeightException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
