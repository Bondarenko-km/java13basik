package DZ1_PM.task6;

import javax.naming.InvalidNameException;
import java.util.Date;
import java.util.GregorianCalendar;

public class FormValidator {

    public static void checkName(String str) throws InvalidNameException {
        if (str.length() < 2 || str.length() > 20) {
            throw new InvalidNameException();
        }
    }

    public static void checkBirthDate(String str) throws InvalidDateException{
        if (!str.matches("[0-3][0-9]\\.[0-9][0-2]\\.[1-2][0|9][0-9][0-9]")) {
            throw new InvalidDateException();
        }
        // Получаем данные дня, месяца, года
        int day = Integer.parseInt(str.substring(0,2));
        int month = Integer.parseInt(str.substring(3,5));
        int year = Integer.parseInt(str.substring(6,10));

        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(new Date());
        int nowDay = calendar.get(GregorianCalendar.DAY_OF_MONTH);
        int nowMonth = calendar.get(GregorianCalendar.MONTH) + 1;
        int nowYear = calendar.get(GregorianCalendar.YEAR);
        if (day > nowDay || day > 31) {
            throw new InvalidDateException();
        }
        if (month > nowMonth) {
            throw new InvalidDateException();
        }
        if (year > nowYear || year < 1900) {
            throw new InvalidDateException();
        }
        if (day / 10 == 3 && day % 10 > 1) {
            throw new InvalidDateException();
        }
        if ((month % 10 == 1 || month % 10 == 3 || month % 10 == 5 || month % 10 == 7 || month % 10 == 8 || month == 10 || month == 12) && day > 30) {
            throw new InvalidDateException();
        }

        if (month % 10 == 2 && day > 28) {
            throw new InvalidDateException();
        }
        if (!calendar.isLeapYear(month) && day > 28) {
            throw new InvalidDateException();
        }
    }

    public static  void checkGender(String str) throws InvalidGenderException{
        if (!str.equalsIgnoreCase(Gender.MALE.toString()) && !str.equalsIgnoreCase(Gender.FEMALE.toString())) {
            throw new InvalidGenderException();
        }

    }
    public static void checkHeight(String str) throws InvalidHeightException {
        if (Double.parseDouble(str) <= 0) {
            throw new InvalidHeightException();
        }
    }
}

