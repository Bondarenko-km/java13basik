package DZ1_PM.task6;

public class InvalidGenderException extends Exception {

    public InvalidGenderException() {
        super("Неверно указан пол");
    }

    public InvalidGenderException(String message) {
        super(message);
    }
}
