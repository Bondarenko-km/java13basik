package DZ1_PM.task3;
//Реализовать метод, открывающий файл ./input.txt
// и сохраняющий в файл .output.txt текст из input,
// где каждый латинский строчный символ заменен на соответствующий заглавный.
// Обязательно использование try с ресурсами.
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Scanner;
public class main {
    private static final String DIRECTORY = "C:\\Users\\User\\IdeaProjects\\DZ2_part1\\src\\DZ1_PM\\task3";
    private static final String OUTPUT = "output.txt";
    private static final String INPUT = "input.txt";

    public static void main(String[] args) {
        try {
            ReadAndWriteInFile();
        } catch (IOException ex) {
            System.out.println("Файл не найден");
        }
    }
    public static void ReadAndWriteInFile() throws IOException {
        Scanner scan = new Scanner(new File(DIRECTORY + "\\" + INPUT));
        Writer writer = new FileWriter(DIRECTORY + "\\" + OUTPUT);
        try(scan; writer) {
            while(scan.hasNext()) {
                writer.write(scan.nextLine().toUpperCase() + "\n");
            }
        }

    }
}
