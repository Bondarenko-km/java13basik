package DZ1_PM.task1;
//Создать собственное исключение MyCheckedException, являющееся проверяемым.

public class MyCheckedException extends Exception {
    public MyCheckedException() {
        super();
    }
    public MyCheckedException(String message) {
        super(message);
    }

}