import java.util.Random;
import java.util.Scanner;
//) Создать программу генерирующую пароль.
//На вход подается число N — длина желаемого пароля. Необходимо проверить,
//что N >= 8, иначе вывести на экран "Пароль с N количеством символов
//небезопасен" (подставить вместо N число) и предложить пользователю еще раз
//ввести число N.
//Если N >= 8 то сгенерировать пароль, удовлетворяющий условиям ниже и
//вывести его на экран. В пароле должны быть:
// заглавные латинские символы
//строчные латинские символы
// числа
public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n;
        do {
            System.out.println("Введите длину пароля");
            n = scanner.nextInt();
            if (n > 0 && n <= 8)
                System.out.println("Пароль с " + n + " количеством символов небезопасен");
        } while (n < 8);
        String s = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_*-";
        StringBuilder password = new StringBuilder();
        for (int i = 0; i < n; i++) {
            Random r = new Random();
            password.append(s.charAt(r.nextInt(s.length())));
            if (password.length() == n
                    && !password.toString().matches("(^(?=.*[a-z]+).*(?=.*[0-9]+).*(?=.*[A-Z]).*(?=.*[*\\-_]).*+$)")) ;
        }
        System.out.println(password);
    }
}