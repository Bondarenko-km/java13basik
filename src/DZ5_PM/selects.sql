--1 По идентификатору заказа получить данные заказа и данные клиента, создавшего этот заказ
select * from orders
join clients on orders.client_id = clients.id
where orders.id = 3;

--2 Получить данные всех заказов одного клиента по идентификатору клиента за последний месяц
select * from orders
where client_id = 1
and order_date >= now() - interval '1 month';

--3 Найти заказ с максимальным количеством купленных цветов, вывести их название и количество
select flowers.flower_type, orders.number_of_flowers from flowers
join orders on flowers.id = orders.flower_id
where orders.number_of_flowers = (select max(orders.number_of_flowers) from orders);

--4 Вывести общую выручку (сумму золотых монет по всем заказам) за все время
select sum(orders.number_of_flowers * flowers.price) from orders
join flowers on flowers.id = orders.flower_id;
