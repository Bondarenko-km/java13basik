create table clients
(
id serial primary key,
 name varchar(30) not null,
mobile varchar(30) not null unique
);


insert into clients(name, mobile)
values('Дмитрий', '8-900-123-45-67');

insert into clients(name, mobile)
values('Анна', '8-900-111-22-33');

insert into clients(name, mobile)
values('Григорий', '8-900-444-55-66');

insert into clients(name, mobile)
values('Аркадий', '8-900-555-66-77');

insert into clients(name, mobile)
values('Ольга', '8-900-999-88-77');

insert into clients(name, mobile)
values('Светлана', '8-900-777-77-88');

select * from clients;