create table orders(
     id serial primary key,
     client_id int references clients(id),
     flower_id int references flowers(id),
     number_of_flowers int check(number_of_flowers >= 1 and number_of_flowers <= 1000),
     order_date timestamp
);

insert into orders(client_id, flower_id, number_of_flowers, order_date)
values(1, 1, 30, now() - interval '15 d');
insert into orders(client_id, flower_id, number_of_flowers, order_date)
values(1, 3, 15, now() - interval '12d');
insert into orders(client_id, flower_id, number_of_flowers, order_date)
values(2, 2, 5, now() - interval '35d');
insert into orders(client_id, flower_id, number_of_flowers, order_date)
values(3, 1, 50, now() - interval '12 h');
insert into orders(client_id, flower_id, number_of_flowers, order_date)
values(4, 1, 3, now() - interval '45d');
insert into orders(client_id, flower_id, number_of_flowers, order_date)
values(5, 2, 1000, now() - interval '25h');
insert into orders(client_id, flower_id, number_of_flowers, order_date)
values(5, 1, 7, now() - interval '3d');
insert into orders(client_id, flower_id, number_of_flowers, order_date)
values(6, 2, 35, now() - interval '6h');

select * from orders;
