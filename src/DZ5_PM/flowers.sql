create table flowers
(
id serial primary key,
    flower_type varchar(30) not null,
    price int not null
);

insert into flowers(flower_type, price)
values('Роза', 100);

insert into flowers(flower_type, price)
values('Лилия', 50);

insert into flowers(flower_type, price)
values('Ромашка', 25);

select * from flowers;
